
package service

import (
    "context"
. "oauth2:JXVrZBNiKoCTnrh-mzzM@gitlab.com/pl-pro/tete/api"

)

// Service type
//  Helloworld Service

        type HelloWorldService struct {
            UnimplementedHelloWorldServiceServer
        }

    // Create a new service
        func NewHelloWorldService() *HelloWorldService {
            service := &HelloWorldService{}
            return service
        }

    //  Sends a greeting
        func (service *HelloWorldService) SayHello(ctx context.Context, request *SayHelloRequest) (*SayHelloResponse, error) {
            return &SayHelloResponse{}, nil
        }
        
    