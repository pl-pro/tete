package main

import (
   "context"
	"fmt"
	"log"
	"net"
	"net/http"

    "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
    
    . "oauth2:JXVrZBNiKoCTnrh-mzzM@gitlab.com/pl-pro/tete/api"
"oauth2:JXVrZBNiKoCTnrh-mzzM@gitlab.com/pl-pro/tete/internal/service"

)

// GRPCRegFunc Used while registering protoc generated gRPC registration function
type GRPCRegFunc func(server *grpc.Server)

func registerGRPCRegFunc(server *grpc.Server, funcs ...GRPCRegFunc) {
	for _, f := range funcs {
		f(server)
	}
}


        func registerHelloWorldService(server *grpc.Server) {
            RegisterHelloWorldServiceServer(server, service.NewHelloWorldService())
        }
    

// GatewayRegFunc Used while registering protoc generated gRPC gateway registration function
type GatewayRegFunc func(context.Context, *runtime.ServeMux) error

func registerGatewayRegFunc(ctx context.Context, mux *runtime.ServeMux, funcs ...GatewayRegFunc) error {
	var err error
	for _, g := range funcs {
		if err = g(ctx, mux); err != nil {
			return err
		}
	}

	return nil
}


        func registerHelloWorldServiceGateway(ctx context.Context, mux *runtime.ServeMux) error {
            opts := []grpc.DialOption{grpc.WithInsecure()}
            return RegisterHelloWorldServiceHandlerFromEndpoint(ctx, mux, "localhost:8080", opts)
        }
    

func startServer(ctx context.Context) error {
	// Create a listener on TCP port
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		return fmt.Errorf("Failed to listen: %+v", err)
	}

	// Create a gRPC server object
	s := grpc.NewServer()
	// register grpc service to server
	registerGRPCRegFunc(s,
        registerHelloWorldService,

	)

	// Serve gRPC server
	log.Println("Serving gRPC on 0.0.0.0:8080")
	go func() {
		_ = s.Serve(lis)
	}()

	return nil
}

func startGateway(ctx context.Context) error {
	mux := runtime.NewServeMux()
	err := registerGatewayRegFunc(ctx, mux,
        registerHelloWorldServiceGateway,

	)
	if err != nil {
		return fmt.Errorf("registerGateway func error: %v", err)
	}

	// Serve gRPC server
	log.Println("GRPC gateway on 0.0.0.0:8081")
	return http.ListenAndServe(":8081", mux)
}

func main() {
	ctx := context.Background()

	// start server
	if err := startServer(ctx); err != nil {
		log.Fatalf("start server error: %v", err)
	}

	// start gateway
	if err := startGateway(ctx); err != nil {
		log.Fatalf("start gateway error: %v", err)
	}
}
