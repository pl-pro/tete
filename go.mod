module oauth2:JXVrZBNiKoCTnrh-mzzM@gitlab.com/pl-pro/tete

go 1.16

require (
	// needed when install binary file
	github.com/axw/gocov v1.0.0
	github.com/bitly/go-simplejson v0.5.0
	github.com/envoyproxy/protoc-gen-validate v0.5.0
	github.com/go-swagger/go-swagger v0.26.1
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/mock v1.5.0
	github.com/golang/protobuf v1.4.3
	github.com/golangci/golangci-lint v1.38.0
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.3.0
	github.com/iamrajiv/helloworld-grpc-gateway v0.0.0-20210427065124-c46da7827451
	github.com/lyft/protoc-gen-star v0.5.2 // indirect
	github.com/pseudomuto/protoc-gen-doc v1.4.1
	github.com/spf13/cast v1.3.1
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.0
	google.golang.org/genproto v0.0.0-20210224155714-063164c882e6
	google.golang.org/grpc v1.36.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.25.1-0.20201208041424-160c7477e0e8
)